Radial Status Indicators
--
This plugin changes the users' status to be a colored ring around their profile picture

You can import this plugin by adding the following line to your enabled file:

`@import 'theme/plugins/RadialStatusIndicators/plugin';`

Original credit goes to [Gibbu](https://github.com/Gibbu/BetterDiscord-Themes/tree/master/RadialStatus)
