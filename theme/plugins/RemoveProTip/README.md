Remove Pro Tip
--
This plugin removes the "Pro Tip"s from showing up

You can import this plugin by adding the following line to your enabled file:

`@import 'theme/plugins/RemoveProTip/plugin';`
