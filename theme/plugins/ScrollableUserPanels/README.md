Scrollable User Panels
--
This plugin allows you to scroll the user panels roles that would usually go off of the screen

You can import this plugin by adding the following line to your enabled file:

`@import 'theme/plugins/ScrollableUserPanels/plugin';`
