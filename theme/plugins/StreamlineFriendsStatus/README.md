Streamline friends status
--
This plugin removes playing/listening/watching/custom statuses from friends list statuses.

Credit: emma 🌺#6334

You can import this plugin by adding the following line to your enabled file:

`@import 'theme/plugins/StreamlineFriendsStatus/plugin';`
